# Task Cyber Army
 Membuat aplikasi sederhana dengan output yang sudah ditentukan

## Database design
 Untuk mempermudah mengolah data dan menghindari redudansi saya membagi table menjadi 4 bagian
 
 - students
 - classrooms
 - grades
 - subjects

![Database esign](https://i.postimg.cc/SKSr9m17/database-design.png)
# Kesulitan 
### task no 2
 Dengan relasi tabel yang kompleks saya kesulitan melakukan query untuk join 3 tabel dengan melakukan filter nilai tertinggi perkelas dan di group berdasarkan nama kelas.
 
 Saat melakukan query yang diatas, saya mendapatkan error seperti berikut 
 ``` 
 Syntax error or access violation: 1055 Error
 ```
Saya berhasil memperbaikinya dengan megubah configurasi di config/database.php
```
'strict' => true,
``` 
menjadi seperti berikut
```
'strict' => false,
``` 

Sumber 
 - [https://stackoverflow.com/questions/40917189/laravel-syntax-error-or-access-violation-1055-error]
 - [https://dev.mysql.com/doc/refman/5.7/en/sql-mode.html#sql-mode-strict]
 - [https://github.com/laravel/framework/issues/14997#issuecomment-242129087]

## Setup
- Clone this repo using `git clone https://gitlab.com/alfarozy.id/task-cyberarmy`
- Goto project dir `cd task-cyberarmy`
- Create .env file `cp .env.example .env`
- Run `composer install`
- Create app key `php artisan key:generate`
- Update db in `.env`
- Run migrations & seeder `php artisan migrate --seed`
- Run `php artisan serve`
- Goto browser `http://localhost:8000`, you should see the app.

## Account
``` 
 E: koji@gmail.com
 P: password
```

#### Dev by me
- Website: [Alfarozy.id](https://alfarozy.id)
- Email  : [mr.alfarozy.a.n@gmail.com](mailto:mr.alfarozy.a.n@gmail.com)
- Ig : [alfarozy_an](instagram.com/alfarozy_an/)
