<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $data = Student::with(['grades','classroom'])->orderBy('name')->get();
        $total_subject = Subject::count();

        return view('backoffice.student.index', compact('data','total_subject'));
    }

    public function create()
    {
        $classrooms = ClassRoom::get();
        return view('backoffice.student.create', compact('classrooms'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required',
            'classroom_id' => 'required'
        ]);

        Student::create($attr);
        return redirect()->route('student.index')->with('msg', 'Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $classrooms = ClassRoom::get();
        $data = Student::findOrFail($id);
        return view('backoffice.student.edit', compact('classrooms', 'data'));
    }

    public function update($id, Request $request)
    {
        $data = Student::findOrFail($id);
        $attr = $request->validate([
            'name' => 'required',
            'classroom_id' => 'required'
        ]);

        $data->update($attr);
        return redirect()->route('student.index')->with('msg', 'Data berhasil ditambahkan');
    }

    public function show($id)
    {
        $data = Student::find($id);
        $total_subject = Subject::count();
        return view('backoffice.student.show', compact('data','total_subject'));
    }
    public function destroy($id)
    {
        Student::destroy($id);
        return redirect()->route('student.index')->with('msg', 'Data berhasil dihapus');
    }
}
