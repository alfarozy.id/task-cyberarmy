<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use Illuminate\Http\Request;

class ClassRoomController extends Controller
{
    public function index()
    {
        $data = ClassRoom::orderBy('name')->get();
        return view('backoffice.classroom.index',compact('data'));
    }

    public function create()
    {
        return view('backoffice.classroom.create');
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' =>'required'
        ]);

        ClassRoom::create($attr);

        return redirect()->route('classroom.index')->with('msg','Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $data = ClassRoom::findOrFail($id);
        return view('backoffice.classroom.edit',compact('data'));
    }

    public function update($id,Request $request)
    {
        $data = ClassRoom::findOrFail($id);
        $attr = $request->validate([
            'name' =>'required'
        ]);

       $data->update($attr);

        return redirect()->route('classroom.index')->with('msg','Data berhasil diperbarui');
    }


    public function destroy($id)
    {
        ClassRoom::destroy($id);
        return redirect()->route('classroom.index')->with('msg','Data berhasil dihapus');
    }
}
