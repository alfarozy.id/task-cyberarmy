<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    public function create()
    {
        if (!request()->id) {
           return redirect()->back();
        }
        $grades = Grade::whereStudentId(request()->id)->get();
        $hasInputed = [];
        if ($grades) {
            
            foreach ($grades as $item) {
                array_push($hasInputed,$item->subject->id);
            }
        }

        $subjects = Subject::whereNotIn("id",$hasInputed)->get();
        return view('backoffice.grade.create', compact('subjects'));
    }

    public function store(Request $request)
    {
        if (!request()->id) {
            return redirect()->back();
        }
        $attr = $request->validate([
            'total' => 'required',
            'subject_id' => 'required',
        ]);

        $attr['student_id'] = htmlspecialchars(request()->id);

        Grade::create($attr);
        return redirect()->route('student.show',$attr['student_id'])->with('msg', 'Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        if (!request()->id) {
            return redirect()->back();
         }
        $data = Grade::findOrFail($id);
        return view('backoffice.grade.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        if (!request()->id) {
            return redirect()->back();
         }
        $data = Grade::find($id);
        $attr = $request->validate([
            'total' => 'required',
        ]);

        $data->update($attr);
        return redirect()->route('student.show',$data->student_id)->with('msg', 'Data berhasil ditambahkan');

    }
}
