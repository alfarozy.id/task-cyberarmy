<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:3'
        ], [
            'email.required' => 'Email wajib',
            'password.required' => 'Password wajib',
            'password.min' => 'Password minimal 3 karakter',
        ]);

        // query user berdasarkan email
        $user = User::where('email', $request->email)->first();

        //cek jika ada user
        if ($user) {

            // cek password
            if (Hash::check($request->password, $user->password)) {
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();
                    return redirect()->intended('dashboard');
                }
            } 
        }
        return redirect()->back()->with('msg', 'Email atau password salah');
    }
    
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerate();
        return redirect('/');
    }
}
