<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $data = Subject::orderBy('name')->get();
        return view('backoffice.subject.index',compact('data'));
    }

    public function create()
    {
        return view('backoffice.subject.create');
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' =>'required'
        ]);

        Subject::create($attr);

        return redirect()->route('subject.index')->with('msg','Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $data = Subject::findOrFail($id);
        return view('backoffice.subject.edit',compact('data'));
    }

    public function update($id,Request $request)
    {
        $data = Subject::findOrFail($id);
        $attr = $request->validate([
            'name' =>'required'
        ]);

       $data->update($attr);

        return redirect()->route('subject.index')->with('msg','Data berhasil diperbarui');
    }


    public function destroy($id)
    {
        Subject::destroy($id);
        return redirect()->route('subject.index')->with('msg','Data berhasil dihapus');
    }
}
