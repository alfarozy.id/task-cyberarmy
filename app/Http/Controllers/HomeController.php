<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\Grade;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $data = Student::addSelect(['total_grade' => Grade::selectRaw('sum(total) as total_grade')->whereColumn('grades.student_id', 'students.id')->groupBy('students.name')])->
        with(['classroom', 'grades'])->when($request->classroom, function ($student) use ($request) {
            return $student->where('classroom_id', $request->classroom);
        })
            ->when($request->filter == 'a-z', function ($student) {
                return $student->orderBy('name', 'ASC');
            })
            ->when($request->filter == 'z-a', function ($student) {
                return $student->orderBy('name', 'DESC');
            })
            ->when($request->filter == 'z-a', function ($student) {
                return $student->orderBy('name', 'DESC');
            })
            ->when($request->filter == 'hight', function ($student) {
                return $student->orderBy('total_grade', 'DESC');
            })
            ->when($request->filter == 'low', function ($student) {
                return $student->orderBy('total_grade', 'ASC');
            })
            ->when($request->search, function ($student) use ($request) {
                return $student->where('name', 'like', '%' . $request->search . '%');
            })
            ->get();

        $classrooms = ClassRoom::get();
        $total_subject = Subject::count();

        return view('frontoffice.index', compact('data', 'total_subject', "classrooms"));
    }
    public function student()
    {
        $total_subject = Subject::count();
        $data = Grade::select('classrooms.name','students.classroom_id')->selectRaw('sum(total) / '.$total_subject.' as total_grade')
        ->join("students",'grades.student_id', 'students.id')
        ->join("classrooms",'students.classroom_id', 'classrooms.id')
        ->groupBy('students.classroom_id')
        ->orderBy('total_grade','DESC')->get();
        return view('frontoffice.student', compact('data', 'total_subject'));
    }

    public function studentShow($id)
    {
        $data = Student::findOrFail($id);
        $total_subject = Subject::count();

        return view('frontoffice.show',compact('data','total_subject'));
    }
}
