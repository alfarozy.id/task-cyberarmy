<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
      .dropdown-toggle::after {
     content: none;
 }
 </style>
    <title>@yield('title') - CyberArmy</title>
  </head>
  <body>
   <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="#">Dataku.id</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ request()->routeIs('homepage.index') ? 'active':'' }}" aria-current="page" href="/">Beranda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ request()->routeIs('homepage.student') ? 'active':'' }}" href="{{ route('homepage.student') }}">Nilai siswa</a>
            </li>
          </ul>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
              <li class="nav-item active"><a class="nav-link" href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
              <li class="nav-item dropdown">
                @if(Auth()->check())
                  <a class="nav-link dropdown-toggle text-white" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth()->user()->name }}</a>
                  <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                     
                    <a class="dropdown-item" href="{{ route('dashboard.index') }}">Dashboard</a>
                      <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                  </div>
                  @else
                  <a class="nav-link dropdown-toggle text-white" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akun</a>
                  <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                     
                      <a class="dropdown-item" href="{{ route('login') }}">login</a>
                  </div>

                @endif
              </li>
          </ul>
      </div>
      </div>
    </nav>
    
   </header>
   <section class="container  mt-5">
    <h3>@yield('title')</h3>
    <hr class="col-2">
    <div class="pt-3">

      @yield('content')
    </div>
    </section>
    <div class="text-center" style="position: fixed;right:10px;bottom:10px;">
      Develope by Alfarozy.id | Laravel v{{ Illuminate\Foundation\Application::VERSION }}
    </div>


    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>
