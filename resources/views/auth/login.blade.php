@extends('layouts.auth')
@section('title', 'Login to system')
@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-lg-5 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                @if (session()->has('msg'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('msg') }}
                                    </div>
                                @endif
                                <h3 class="mb-5">Login</h3>
                                <form action="{{ route('login') }}" method="POST">
                                    @csrf

                                    <div class="form-group mb-3">
                                        <input type="email" name="email"
                                            class="form-control form-control @error('email') is-invalid @enderror"
                                            id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email" />

                                        @error('email')
                                            <small class="text-danger ml-1">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-3">
                                        <input name="password" type="password"
                                            class="form-control form-control @error('password') is-invalid @enderror"
                                            placeholder="Password" />
                                        @error('password')
                                            <small class="text-danger ml-1">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <button class="btn btn-primary col-lg-12 mt-5" type="submit">Login</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
