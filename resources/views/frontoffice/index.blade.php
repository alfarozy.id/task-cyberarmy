@extends('layouts.frontoffice')

@section('title','Beranda')
@section('content')
   

<div class="d-flex mb-3 justify-content-end">
    <!-- Example single danger button -->
   
    <div class="col  mr-2">

      <div class="btn-group ">
        <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          @if (request()->classroom)
          Kelas {{$classrooms[0]->name}}
          @else
          Semua kelas
          @endif
        </button>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="/">Semua kelas</a></li>
          @foreach ($classrooms as $item)
          
          <li><a class="dropdown-item" href="/?classroom={{ $item->id }}">{{ $item->name}}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-lg-6">
      <form action="" method="GET">
        <div class="mb-3">
          <input type="search" name="search" value="{{ request()->search }}" autocomplete="off" class="form-control" id="search" placeholder="Cari data siswa...">
        </div>
      </form>
    </div>
    <div class="col text-end">

    <div class="btn-group">
      <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
        Filter Data : 
        @if (request()->filter == 'a-z')
        Nama (A - Z)
        @elseif(request()->filter == 'z-a')
        Nama (Z - A)
        @elseif(request()->filter == 'hight')
        Nilai tertinggi
        @elseif(request()->filter == 'low')
        Nilai terendah
        @else
        Default
        @endif
      </button>
      <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="/?filter=default">Default</a></li>
        <li><a class="dropdown-item" href="/?filter=a-z">Nama (A - Z)</a></li>
        <li><a class="dropdown-item" href="/?filter=z-a">Nama (Z - A)</a></li>
        <li><a class="dropdown-item" href="/?filter=hight">Nilai tertinggi</a></li>
        <li><a class="dropdown-item" href="/?filter=low">Nilai terendah</a></li>
      </ul>
    </div>
  </div>


</div>
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col" width="5%">#</th>
      <th scope="col" width="30%">Nama</th>
      <th scope="col" width="30%">Kelas</th>
      <th scope="col" width="25%" class="text-center">Nilai</th>
      <th scope="col" width="10%" class="text-center">Nilai</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($data as $item)
        
    <tr>
      <th scope="row">{{ $loop->iteration }}</th>
      <td>{{ $item->name }}</td>
      <td>{{ $item->classroom->name }}</td>
      <td class="text-center">{{ $item->grades->sum('total') / $total_subject }}</td>
      <td>
        <a href="{{ route('homepage.student.show',$item->id) }}" class="btn btn-sm btn-light border"> Detail Nilai</a>
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="4" class="text-center"><h5>Data tidak tersedia</h5></td>
    </tr>
    @endforelse
   
  </tbody>
</table>
@endsection