@extends('layouts.frontoffice')

@section('title', 'Detail nilai siswa')
@section('content')

    <div class="mb-3 justify-content-start">
        <!-- Example single danger button -->
        <h6>Nama : {{ $data->name}}</h6>
        <h6>Kelas : {{ $data->classroom->name}}</h6>
        <h6>Total nilai : {{ $data->grades->sum('total') / $total_subject}}</h6> 
        {{-- total nilai = rata2 semua nilai --}}
    </div>
    <table class="table table-bordered mt-5">
        <thead>
            <tr>
                @foreach ($data->grades as $item)
                    <td class="text-center">{{ $item->subject->name }}</td>
                @endforeach
            </tr>
        </thead>
        <tbody>

            <tr>
                @foreach ($data->grades as $item)
                    <td class="text-center">{{ $item->total }}</td>
                @endforeach
            </tr>


        </tbody>
    </table>
@endsection
