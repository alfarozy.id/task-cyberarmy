@extends('layouts.frontoffice')

@section('title', 'Nilai siswa perkelas')
@section('content')

    <div class="d-flex mb-3 justify-content-end">
        <!-- Example single danger button -->

    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Kelas</th>
                <th scope="col">Nama</th>
                <th scope="col">Nilai</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                @php
                    $total_nilai_kelas = 0;
                    $students = \App\Models\Student::whereClassroomId($item->classroom_id)->get();
                @endphp
                <tr>
                    <td rowspan="{{ $students->count() + 2 }}">Kelas {{ $item->name }}</td>
                </tr>

                {{-- looping siswa perkelas filter dengan nilai tertinggi  --}}
                @foreach ($students->sortByDesc(function ($student) {
            return $student->grades->sum('total');
        }) as $student)
                    @php
                        $nilai_rata2 = $student->grades->sum('total') / $total_subject;
                        $total_nilai_kelas += $nilai_rata2;
                    @endphp
                    <tr>
                        <td>{{ $student->name }}</td>
                        <td>{{ $nilai_rata2 }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td><b>{{ $total_nilai_kelas }}</b></td>
                </tr>
            @endforeach

        </tbody>
    </table>
@endsection
