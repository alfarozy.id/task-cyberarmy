@extends('layouts.backoffice')
@section('title', 'Detail siswa')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Detail siswa</h4>
        @if (session()->has('msg'))
            
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('msg')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <div class="row my-3 text-secondary">
            <h5> Nama : {{ $data->name }}</h5>
            <p> Kelas : {{ $data->classroom->name}}</p>
        </div>
        
        <div class="row justify-content-between my-3">
            
            <div class="col-lg-2">

                <a href="{{ route('student.index') }}" class="btn btn-secondary"> <i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            
            @if ($total_subject > $data->grades->count())
            <div class="col-lg-2">

                <a href="{{ route('grade.create',['id'=>$data->id]) }}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah nilai</a>
            </div>
            @endif
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col" width="70%">Mapel</th>
                    <th width="10%"> Total nilai</th>
                    <th scope="col" width="20%" class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($data->grades as $item)
                    <tr>
                        <th scope="row">{{ $item->subject->name }}</th>
                        <td class="text-center">{{ $item->total }}</td>
                        <td class="text-center">
                            <a href="{{ route('grade.edit',$item->id) }}?id={{$data->id }}" class="btn btn-sm btn-secondary text-uppercase"> <i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                @empty
                <tr>
                    <th colspan="3" class="text-center"> <h4>Nilai belum diinput</h4></th>
                </tr>
                @endforelse
                <tr class="border-white">
                    <th colspan="1" class="text-end">Total nilai </th>
                    <td class="text-center">{{ $data->grades->sum('total') }}</td>
                    <td></td>
                </tr>
                <tr class="border-white">
                    <th colspan="1" class="text-end">Rata - rata </th>
                    <td class="text-center">{{ $data->grades->sum('total') / $total_subject }}</td>
                    <td></td>
                </tr>

            </tbody>
        </table>
    </div>
@endsection
