@extends('layouts.backoffice')
@section('title', 'Edit Data')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Edit data</h4>

        <div class="container p-3">

            <form method="POST" action="{{ route('student.update', $data->id) }}">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label class="form-label">Nama lengkap</label>
                    <input type="text" name="name" autocomplete="off" value="{{ $data->name }}" class="form-control">
                    @error('name')
                        <small class="text-danger"> {{ $message }}</small>
                    @enderror
                </div>
                <div class="mb-3">
                    <label id="classroom" class="form-label">Kelas</label>
                    <select class="form-control" name="classroom_id" id="classroom">
                        <option value=""> -- Pilih Kelas --</option>
                        @foreach ($classrooms as $item)
                            <option value="{{ $item->id }}" {{ $data->classroom_id == $item->id ? 'selected' : '' }}>
                                {{ $item->name }}</option>
                        @endforeach
                    </select>
                    @error('classroom_id')
                        <small class="text-danger"> {{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
