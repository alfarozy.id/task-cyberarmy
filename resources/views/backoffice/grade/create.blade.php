@extends('layouts.backoffice')
@section('title','Input nilai')
@section('content')
    
<div class="container-fluid p-4">
    <h4 class="mt-4">Input Nilai</h4>
   
    <div class="container p-3">

        <form method="POST" action="{{ route('grade.store',['id'=>request()->id]) }}">
            @csrf
            
            <div class="mb-3">
                <label id="subject" class="form-label">Mata pelajaran</label>
                <select class="form-control" name="subject_id" id="subject">
                    <option value=""> -- Pilih mapel --</option>
                    @foreach ($subjects as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('subject_id')
                <small class="text-danger"> {{ $message }}</small>
            @enderror
            </div>
            <div class="mb-3">
                <label class="form-label">Total nilai</label>
                <input type="number" name="total" autocomplete="off" class="form-control">
                @error('total')
                    <small class="text-danger"> {{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('student.show',request()->id) }}" class="btn btn-secondary">kembali</a>
        </form>
    </div>
</div>
@endsection