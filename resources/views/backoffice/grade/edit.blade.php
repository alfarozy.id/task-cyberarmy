@extends('layouts.backoffice')
@section('title','Edit nilai')
@section('content')
    
<div class="container-fluid p-4">
    <h4 class="mt-4">Edit Nilai</h4>
   
    <div class="container p-3">

        <form method="POST" action="{{ route('grade.update',$data->id)}}?id={{ request()->id }}">
            @csrf
@method('put')
            
            <div class="mb-3">
                <label id="subject" class="form-label">Mata pelajaran </label>
                <input type="text" readonly value="{{ $data->subject->name }}" autocomplete="off" class="form-control">
              
            </div>
            <div class="mb-3">
                <label class="form-label">Total nilai</label>
                <input type="number" name="total" value="{{ $data->total }}" autocomplete="off" class="form-control">
                @error('total')
                    <small class="text-danger"> {{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('student.show',request()->id) }}" class="btn btn-secondary">kembali</a>
        </form>
    </div>
</div>
@endsection