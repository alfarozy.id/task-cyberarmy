@extends('layouts.backoffice')
@section('title', 'Tambah Data')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Tambah data</h4>

        <div class="container p-3">

            <form method="POST" action="{{ route('subject.store') }}">
                @csrf
                <div class="mb-3">
                    <label class="form-label">Mata pelajaran</label>
                    <input type="text" name="name" autocomplete="off" class="form-control">
                    @error('name')
                        <small class="text-danger"> {{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
