@extends('layouts.backoffice')
@section('title', 'Edit Data')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Edit data</h4>

        <div class="container p-3">

            <form method="POST" action="{{ route('classroom.update', $data->id) }}">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="kelass" class="form-label">Kelas</label>
                    <input type="text" name="name" class="form-control text-uppercase" value="{{ $data->name }}"
                        id="exampleInputEmail1">
                    @error('name')
                        <small class="text-danger"> {{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
