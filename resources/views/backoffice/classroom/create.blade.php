@extends('layouts.backoffice')
@section('title', 'Tambah Data')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Tambah data</h4>

        <div class="container p-3">

            <form method="POST" action="{{ route('classroom.store') }}">
                @csrf
                <div class="mb-3">
                    <label for="kelass" class="form-label">Kelas</label>
                    <input type="text" name="name" autocomplete="off" class="form-control" id="exampleInputEmail1">
                    @error('name')
                        <small class="text-danger"> {{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
