@extends('layouts.backoffice')
@section('title', 'Data Kelas')
@section('content')

    <div class="container-fluid p-4">
        <h4 class="mt-4">Data kelas</h4>
        @if (session()->has('msg'))
            
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('msg')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <div class="row justify-content-end my-3">
        
            <div class="col-lg-2">

                <a href="{{ route('classroom.create') }}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah data</a>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" width="5%">#</th>
                    <th scope="col" width="80%">Kelas</th>
                    <th scope="col" width="15%" class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $item->name }}</td>
                        <td class="text-center">
                            <a href="{{ route('classroom.edit',$item->id) }}" class="btn btn-sm btn-secondary text-uppercase"> <i class="fa fa-edit"></i></a>
                            
                            <form class="d-inline" action="{{ route('classroom.destroy',$item->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-sm btn-danger"> <i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
@endsection
