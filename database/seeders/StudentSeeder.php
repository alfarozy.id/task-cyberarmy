<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
            'name'=>"Satria",
            'classroom_id'=>1
        ]);
        Student::create([
            'name'=>"Rio",
            'classroom_id'=>1
        ]);
        Student::create([
            'name'=>"Santi",
            'classroom_id'=>3
        ]);
        Student::create([
            'name'=>"Rani",
            'classroom_id'=>2
        ]);
        Student::create([
            'name'=>"Andi",
            'classroom_id'=>1
        ]);
        Student::create([
            'name'=>"Yuni",
            'classroom_id'=>2
        ]);
        Student::create([
            'name'=>"Vera",
            'classroom_id'=>3
        ]);
    }
}
