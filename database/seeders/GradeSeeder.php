<?php

namespace Database\Seeders;

use App\Models\Grade;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //> satria
        Grade::create([
            'student_id' => 1,
            'subject_id' => 1,
            'total' => 90
        ]);
        Grade::create([
            'student_id' => 1,
            'subject_id' => 2,
            'total' => 88
        ]);
        Grade::create([
            'student_id' => 1,
            'subject_id' => 3,
            'total' => 100
        ]);
        Grade::create([
            'student_id' => 1,
            'subject_id' => 4,
            'total' => 82
        ]);

        //> rio
        Grade::create([
            'student_id' => 2,
            'subject_id' => 1,
            'total' => 89
        ]);
        Grade::create([
            'student_id' => 2,
            'subject_id' => 2,
            'total' => 90
        ]);
        Grade::create([
            'student_id' => 2,
            'subject_id' => 3,
            'total' => 84
        ]);
        Grade::create([
            'student_id' => 2,
            'subject_id' => 4,
            'total' => 85
        ]);

        //> Santi
        Grade::create([
            'student_id' => 3,
            'subject_id' => 1,
            'total' => 80
        ]);
        Grade::create([
            'student_id' => 3,
            'subject_id' => 2,
            'total' => 90
        ]);
        Grade::create([
            'student_id' => 3,
            'subject_id' => 3,
            'total' => 87
        ]);
        Grade::create([
            'student_id' => 3,
            'subject_id' => 4,
            'total' => 83
        ]);
        //> Rani
        Grade::create([
            'student_id' => 4,
            'subject_id' => 1,
            'total' => 80
        ]);
        Grade::create([
            'student_id' => 4,
            'subject_id' => 2,
            'total' => 85
        ]);
        Grade::create([
            'student_id' => 4,
            'subject_id' => 3,
            'total' => 77
        ]);
        Grade::create([
            'student_id' => 4,
            'subject_id' => 4,
            'total' => 78
        ]);
        //> Andi
        Grade::create([
            'student_id' => 5,
            'subject_id' => 1,
            'total' => 75
        ]);
        Grade::create([
            'student_id' => 5,
            'subject_id' => 2,
            'total' => 74
        ]);
        Grade::create([
            'student_id' => 5,
            'subject_id' => 3,
            'total' => 77
        ]);
        Grade::create([
            'student_id' => 5,
            'subject_id' => 4,
            'total' => 74
        ]);
        //> Yuni
        Grade::create([
            'student_id' => 6,
            'subject_id' => 1,
            'total' => 74
        ]);
        Grade::create([
            'student_id' => 6,
            'subject_id' => 2,
            'total' => 74
        ]);
        Grade::create([
            'student_id' => 6,
            'subject_id' => 3,
            'total' => 72
        ]);
        Grade::create([
            'student_id' => 6,
            'subject_id' => 4,
            'total' => 76
        ]);
        //> Vera
        Grade::create([
            'student_id' => 7,
            'subject_id' => 1,
            'total' => 70
        ]);
        Grade::create([
            'student_id' => 7,
            'subject_id' => 2,
            'total' => 70
        ]);
        Grade::create([
            'student_id' => 7,
            'subject_id' => 3,
            'total' => 68
        ]);
        Grade::create([
            'student_id' => 7,
            'subject_id' => 4,
            'total' => 72
        ]);
    }
}
