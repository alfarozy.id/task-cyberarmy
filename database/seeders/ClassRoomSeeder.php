<?php

namespace Database\Seeders;

use App\Models\ClassRoom;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClassRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClassRoom::create([
            'name' => '3A',
        ]);
        ClassRoom::create([
            'name' => '3B',
        ]);
        ClassRoom::create([
            'name' => '3C',
        ]);
    }
}
