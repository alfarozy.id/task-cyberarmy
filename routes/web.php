<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClassRoomController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//> frontoffice
Route::get('/',[HomeController::class,'index'])->name('homepage.index');
Route::get('/student',[HomeController::class,'student'])->name('homepage.student');
Route::get('/student/{id}',[HomeController::class,'studentShow'])->name('homepage.student.show');

//> backoffice
Route::prefix("/dashboard")->middleware(['auth'])->group(function(){

    Route::get('/',[DashboardController::class,'index'])->name('dashboard.index');

    //> Core module
    Route::resource('classroom',ClassRoomController::class)->except('show');
    Route::resource('subject',SubjectController::class)->except('show');
    Route::resource('student',StudentController::class);
    Route::resource('grade',GradeController::class)->only(['create','store','update','edit']);

    Route::get("/logout",[AuthController::class,'logout'])->name('logout');
});

//> Auth
Route::middleware(['guest'])->group(function () {

    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'authenticate'])->name('login');
});